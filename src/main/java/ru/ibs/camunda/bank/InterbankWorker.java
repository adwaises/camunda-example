package ru.ibs.camunda.bank;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.ExternalTaskClient;

@Slf4j
public class InterbankWorker {

    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(10000)
                .build();

        client.subscribe("interbankWorker")
                .lockDuration(1000)
                .handler((externalTask, externalTaskService) -> {
                    String details = (String) externalTask.getVariable("details");
                    Long amount = (Long) externalTask.getVariable("amount");
                    log.info(">>>>>>>>>>>>>> Begin transaction [Amount: {}, Details:{}]", amount, details);
                    log.info(">>>>>>>>>>>>>> End transaction [Amount: {}, Details:{}]", amount, details);

                    externalTaskService.complete(externalTask);
                }).open();
    }
}
